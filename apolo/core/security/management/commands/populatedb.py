from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django_seed import Seed

class Command(BaseCommand):
    help = 'Populate Database'

    def seeder_database(self):
        User = get_user_model()
        #create_user(username, email, name,last_name, password, False, False, **extra_fields)
        admin = User.objects.create_user('admin', 'admin@admin.com', 'admin', **{
                                         "is_active": True, "is_staff": True, "is_superuser": True})
        staff = User.objects.create_user(
            'staff', 'staff@staff.com', 'staff', **{"is_active": True, "is_staff": True})
        user = User.objects.create_user(
            'user', 'user@user.com', 'user', **{"is_active": True})

        

    def handle(self, *args, **options):
        self.seeder_database()